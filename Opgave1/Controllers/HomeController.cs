﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Opgave1.Models;

namespace Opgave1.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost]
        public IActionResult Create(TilmeldingerModel model)
        {
            string message = "";

            if (ModelState.IsValid)
            {
                message = "Hej " + model.Fornavn + " " + model.Efternavn + " fra " + model.Firma + ". Du er nu tilmeldt arrangementet";
            }
            else
            {
                message = "Der opstod en fejl. Prøv igen!";
            }
            return Content(message);
        }
    }
}